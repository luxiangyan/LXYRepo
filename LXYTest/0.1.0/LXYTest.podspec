#
# Be sure to run `pod lib lint LXYTest.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LXYTest'
  s.version          = '0.1.0'
  s.summary          = 'lxylxy of LXYTest.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/luxiangyan/LXYTest'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'LXY' => '1556937213@qq.com' }
  s.source           = { :git => 'https://gitee.com/luxiangyan/LXYTest.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.3'

  s.swift_version = '4.2'
  s.source_files = 'LXYTest/Classes/**/*'

end
